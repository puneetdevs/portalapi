## About Portal API

The APP uses email ID to authenticate the user, and send them a login link with unique Token. The Token is then sent as an Athorization Bearer header and authenticated from the server for every request.

The Frontend APP does not communicate with the DB directly.

## Portal API List

The API is hosted on https://portalapi.customerdemo.info

- http://portalapi.customerdemo.info/api/auth/login
```
Method Type: POST

Headers:
- X-Requested-With: XMLHttpRequest

Body:

email: test@gmail.com

Response Format:
{
    "access_token": "<Token>",
    "token_type": "bearer",
    "expires_in": 1200
}

```

- http://portalapi.customerdemo.info/api/auth/me

```
Method Type: POST

Headers:
- X-Requested-With: XMLHttpRequest
- Authorization: Bearer <Token>

Response Format:
{
    "name": "Test",
    "email": "test@gmail.com",
    "created_at": null,
    "updated_at": "2020-01-23 04:39:30"
}

```

- http://portalapi.customerdemo.info/api/auth/logout

```
Method Type: POST

Headers:
- X-Requested-With: XMLHttpRequest
- Authorization: Bearer <Token>

Response Format:
{
    message: "Successfully logged out"
}

```
## Steps to API Setup

- Update the Database credentials in .env file
- Update the SMTP credentials in .env file (You can use mailtrap.io for viewing the email with Login Link)
- Run Composer Install command
- Run php artisan migrate
- Run php artisan db:seed

**NOTE:** Test email address is ***test@gmail.com***
