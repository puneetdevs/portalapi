<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CheckLoginToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = auth()->getToken();
        $user = User::where('token', $token)->first();
        
        if($user){
            return $next($request);
        }
        
        return response()->json(['error' => 'Token is invalid.'], 404);
    }
}
