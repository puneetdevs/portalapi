<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserLoginLinkNotification extends Notification
{
    use Queueable;

    public $token;

    public $uuid;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $uuid)
    {
        $this->token = $token;
        $this->uuid  = $uuid;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Login Link')
                    ->line('Please Login to Portal API by clicking on the button below.')
                    ->line('The link will expire in 20 minutes.')
                    ->action('Login', 'https://portalfront.customerdemourl.com/auth/'.$this->token.'/'.$this->uuid)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
