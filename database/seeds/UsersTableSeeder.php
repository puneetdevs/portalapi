<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'    => Str::uuid(),
            'name' => 'Test',
            'email' => 'puneet.singla@mailinator.com',
        ]);
    }
}
