import React from 'react';

export App = () => (
    const user  = useUser()
    return user ? <AuthenticatedApp /> : <UnauthenticatedApp />
);

